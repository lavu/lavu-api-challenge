# Lavu API Challenge

## Background
Lavu provides SaaS products in the restaurant industry. One of this products is an Inventory Management System. 

Inventory Managment Systems are generally used to track inventory levels, orders, purchases and sales.

## The Challenge
Provide a RESTful API that respects/implements basic Inventory Management functionality. More specifically implement the concept of _Inventory Levels_, utilize CSV (data/inventory-levels-data.csv) as seed data.

The API should allow any client consuming it to perform the following:
* CRUD operations on inventory levels.
* Increment/decrement inventory levels.

It is important that you treat your submission as if it were going into production. 

Your submission should include a link to your repository, documentation on how to access the API(s) and the expected input for any endpoint provided.

You are free to use any tools at your disposal to accomplish your solution.
